const socket = io()

let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')

// Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}


// Si un pseudo existe déjà, demander le changement.
if (username == "pseudo"){
    username = prompt('Ce pseudo existe déjà !')
}

//si changement de pseudo, remplacer dans la liste
socket.emit('newPseudo', username)
    form.addEventListener('renommer', function () {
        e.preventDefault()
        document.getElementById('renommer').addEventListener("click")
});

// Envoi du nouveau user.
socket.emit('newUser', username)
//Socket On est une méthode qui prend en compte un paramètre.
//Lorsque newUser, j'ajoute un username dans ma variable tableau Users.


// Envoi d'un message
form.addEventListener('submit', function (e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;
        const date = new Date();
        socket.emit('newMessage', username, msg, date)
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function (username, msg, date) {
    var item = document.createElement('li')
    item.textContent = username + ': ' + msg + ' - ' + date
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des nouvelles notifications
socket.on('newNotification', function (msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function (users) {
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log(users, 'Users list');

    for (let i = 0; i < users.length; i++) {
        console.log(users[i].username);
        let node = document.createElement("LI");                 // Create a <li> node
        let textnode = document.createTextNode(users[i].username);         // Create a text node
        node.appendChild(textnode);                              // Append the text to <li>
        document.getElementById("users").appendChild(node);
    }
})