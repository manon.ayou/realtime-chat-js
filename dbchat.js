const MongoClient = require('mongodb').MongoClient;

const url = "mongodb://localhost:27017/chat";

// Creation database and 'messages' collection
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    let database = db.db("chat");
    database.createCollection("messages", function(err, res) {
        if (err) throw err;
        console.log("Collection 'messages 'created!");
    });
    console.log("Database 'chat' created!");
    db.close();
});